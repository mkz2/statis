function formatDate(dateParam,format="yyyy/MM/dd") {
  const dateStr =  new Date(dateParam|| Date.now()).toLocaleString("zh-CN")
  if(dateStr === 'Invalid Date') return dateParam
  const dateF = {0: 'y',1: 'M', 2 :'d',3:'h',4:'m',5: 's'}
  const dateEntry = dateStr.split(/[\s\/:]/).map((item,idx) => {
      return [dateF[idx], (item || '').padStart(2, '0')]
  })
  const dateFormatObj = Object.fromEntries(dateEntry)
  return format.replace(/(\w+)/g,(m='') => dateFormatObj[m[0]])
}
const experience = [
  {name: '四方精创', start: '2019/03-04 00:00:00', end: '至今', age: '0', today: '0',toMonth: '0', toYear: '0'},
  {name: '大学', start: '2016/09/10 00:00:00', end: '2019/06/26 00:00:00', age: '0', today: '0',toMonth: '0', toYear: '0'},
  {name: '高中', start: '2013/09/01 00:00:00', end: '2016/06/07 00:00:00', age: '0', today: '0',toMonth: '0', toYear: '0'},
  {name: '中学', start: '2010/09/01 00:00:00', end: '2013/06/16 00:00:00', age: '0', today: '0',toMonth: '0', toYear: '0'},
  {name: '小学', start: '2004/09/01 00:00:00', end: '2010/07/01 00:00:00', age: '0', today: '0',toMonth: '0', toYear: '0'},
]
const birthDateTime = new Date("1996/10/29 17:50:00").getTime()
function getData(item) {
const _start = new Date(item.start)
const _end = new Date(item.end === '至今' ? item.start : item.end)
const age = Math.floor(((_start.getTime() || 0) - birthDateTime) /31536000000)
const endTime = Date.now() - _end.getTime()
const today = Math.floor(endTime / 86400000)
const toYear = Math.floor(endTime / 31536000000)
const toMonth = new Date().getMonth() - _end.getMonth() +(12 * (new Date().getFullYear() - _end.getFullYear()))
return {
  ...item,
  start:  formatDate(item.start),
  end:  formatDate(item.end),
  age,
  today,
  toMonth,
  toYear
}
}
const trs = experience.map(item => {
const data = getData(item)
return `<tr><td>${data.name}（${data.start}~${data.end}）</td><td>${data.age}岁，至今：${data.today}天-${data.toMonth}月-${data.toYear}年</td></tr>`
})
const template = `
<table>
${trs.join('')}
</table>
`
document.querySelector('#experience').innerHTML=template
